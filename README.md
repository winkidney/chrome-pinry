Pinry Chrome
============

This is a Chrome Extension to ease adding content to [Pinry](https://github.com/pinry/pinry/).

Compile your own or install from [Google Play Store](https://chrome.google.com/webstore/detail/pinry-chrome/jmhdcnmfkglikfjafdmdikoonedgijpa).
